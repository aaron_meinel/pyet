import sys

from lark import Lark, Transformer, v_args


diet_grammar = r"""
?start: meal+
?meal: ("Breakfast" | "Lunch" | "Dinner" | "Snack") _NL portion+
?portion: " "~4 food " "~4 weight _NL+
?food: WORD NUMBER*
?weight: NUMBER

%import common.WORD
%import common.NUMBER
%import common.WS_INLINE
%import common.WS
%import common.NEWLINE -> _NL
%import common.DIGIT
%import common.LETTER

%ignore _NL
"""
# The last ignore statement in the grammar could be dangerous, i do not quite understand yet whether terminals that are in the actual grammar will also be ignored.

class Food(object):
    """
    Helper class to make unique objects for each food entry in the
    journal.  Their values are then summed up in `parseDiet` and the
    keys are transformed back to simple strings for easier data
    processing.
    """
    def __init__(self,name):
        self.name = name

    def __str__(self):
        return self.name


class MyTransformer(Transformer):

    def meal(self,items):

        return dict(items)

    def portion(self, items):
        return Food(str(items[0])), float(items[1])

    def food(self, items):
        return "".join(items)

def parseDiet(journal_string):
    parser = Lark(diet_grammar)
    tree = parser.parse(journal_string)
    T = MyTransformer()
    B = T.transform(tree)
    result = dict.fromkeys(map(lambda x: x.name, B.keys()), 0.0)
    for key, val in B.items():
        result[key.name] += val
    return result



