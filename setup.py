from setuptools import setup, find_packages

setup(
    name = 'pyet',
    version = '0.1',
    packages = find_packages(),

     # metadata to display on PyPI
    author="Aaron Meinel",
    #author_email="me@example.com",
    description="Monitor your diet (i.e. the amounts of nutrients you ingest) using plaintext. It is a bit like https://www.ledger-cli.org/ just not as big and well implemented.",

    keywords="diet nutrition PIM health",
    url="https://gitlab.com/aaron_meinel/pyet",   # project home page, if any
    scripts = ['bin/pyet']
)
